<?php

class UserData {
    public $userName;
    public $exerciseNumber;

    public function __construct($userName, $exerciseNumber) {
        $this->userName = $userName;
        $this->exerciseNumber = $exerciseNumber;
    }
}

